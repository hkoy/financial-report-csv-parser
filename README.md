Stwórz moduł, który parsuje plik CSV.

## Kryteria akceptacji
* stworzono kontroler (nazwa dowolna), który przyjmuje submit z przykładowego formularza:
```html
<form method="post" enctype="multipart/form-data">
  <input type="file" name="csvFile">
  <input type="submit" value="Parse File" name="submit">
</form>
```
* plik jest sprawdzany, czy jest plikiem csv
* sprawdzana jest również poprawność danych (data):
  * w danym wierszu sprawdzane jest czy kolumna `Date` zgadza się z kolumnami `Month Number`, `Month Name`, `Year`
* na ekran wypisywany jest log ze sprawdzenia pliku:
  * wypisywane są wiersze, w których pole daty było błędne
* na ekran wypisywane jest podsumowanie (suma) zysków sprzedaży (kolumna `Profit`) - błędne wiersze nie są brane pod uwagę:
  * pogrupowane wg kraju (kolumna `Country`) (sortowane alfabetycznie)
  * pogrupowane wg segmentu (kolumna `Segment`) (sortowane wg wielkości sprzedaży)
* zachowano standardy dobrego programowania SOLID
* zachowano wzorzec MVC
* obsługa może być z widoku lub komendy symfony - tutaj jest dowolność
* UnitTesty mile widziane ;)

## Przykład
#### Przykładowy plik
[Financial_Sample.csv](/uploads/10f4830b6699e0fe75c5bf8dd620c0c1/Financial_Sample.csv)

#### Spodziewany wynik
```
Parsing Log:
2. row : Wrong column 'Month Name'. 


Financial report

By countries:
France: $ 10 890,00
Germany: $ 17 650,00

By segment:
Midmarket: $ 15 330,00
Government: $ 13 210,00
```
