<?php

namespace App\Controller;

use App\Form\FinancialParserCSVType;
use App\Service\ParseCSVFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function parse(Request $request, ParseCSVFile $parser): Response
    {
        $form = $this->createForm(FinancialParserCSVType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $parser->parse($form->get('csvFile')->getData());
            $date_errors = $parser->getErrors();
            $data_by_country = $parser->getFinancialReport('Country','Country','asc');
            $data_by_segment = $parser->getFinancialReport('Segment','Profit','desc');

            return $this->render('home/result.html.twig', [
                'data_by_country' => $data_by_country,
                'data_by_segment' => $data_by_segment,
                'date_errors' => $date_errors,
            ]);
        }
        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
