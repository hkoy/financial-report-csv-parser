<?php

namespace App\Service;

use DateTime;

class ParseCSVFile
{
    private array $data = [];
    private array $headers = [];
    private array $errors = [];

    public function parse(string $file): ?array
    {
        // Auto detect line endings
        // todo: use a better way to detect line endings
        ini_set('auto_detect_line_endings', true);

        $row_no = 0;
        // If the file can be opened as readable, bind a named resource
        if (($handle = fopen($file, 'r')) !== FALSE) {
            // Loop through each row
            while (($row = fgetcsv($handle, null, ';')) !== FALSE) {
                $row_no++;
                // Loop through each field
                foreach ($row as &$field) {
                    // Remove any invalid or hidden characters
                    $field = trim(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $field));
                }

                // If the header has been stored
                if ($this->headers) {
                    $t_row = array_combine($this->headers, $row);
                    if ($this->validateDate($t_row, $row_no)) {
                        $this->data[] = array_combine($this->headers, $row);
                    }
                    unset($t_row);
                } // Else the header has not been stored
                else {
                    // Store the current row as the header
                    $this->headers = $row;
                }
            }

            // Close the file pointer
            fclose($handle);
        }
        return $this->data ?: null;
    }

    public function validateDate(array $row, int $row_no): bool
    {
        static $date_keys = [
            'n' => 'Month Number',
            'F' => 'Month Name',
            'Y' => 'Year',
        ];

        $dt = DateTime::createFromFormat('d/m/Y', $row['Date']);
        if ($dt === false) {
            $this->errors[] = "$row_no. row: Wrong column 'Date'.";
            return false;
        }

        foreach ($date_keys as $keyword => $title) {
            if ($dt->format($keyword) !== $row[$title]) {
                $this->errors[] = "$row_no. row: Wrong column '$title'.";
                return false;
            }
        }

        return true;
    }

    public function getFinancialReport(string $column, string $sort_by, string $sort): array
    {
        $data = $this->data;
        $reportData = [];

        foreach ($data as &$row) {
            $row['Profit'] = intval(filter_var($row['Profit'], FILTER_SANITIZE_NUMBER_FLOAT,
                FILTER_FLAG_ALLOW_FRACTION
            ));
            if (!isset($reportData[$row[$column]])) {
                $reportData[$row[$column]] = 0;
            }
            $reportData[$row[$column]] += $row['Profit'];
        }

        if ($sort_by === 'Country') {
            if ($sort === 'asc') {
                ksort($reportData);
            } else {
                krsort($reportData);
            }
        } elseif ($sort_by === 'Profit') {
            if ($sort === 'asc') {
                asort($reportData);
            } else {
                arsort($reportData);
            }
        }

        foreach ($reportData as &$data) {
            $data = number_format($data / 100, 2, ',', ' ');
        }
        return $reportData;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}